<?php
namespace app\controllers;

use app\core\Controller;
use app\lib\Db;

class MainController extends Controller
{
    public function setLayout()
    {
        $this->view->layout = 'test';
    }

    public function index()
    {
//        $this->view->layout = 'custom';           //подключаем шаблон
        $this->view->patch = 'index';       //задаем путь к views не зависемо от URL
//        $db = new Db;
//
//        $params = [
//            'id' => 2,
//        ];
//
//        $data = $db->row('SELECT title FROM news WHERE id = :id', $params);
//        debug($data);die;
        $main = $this->loadModel('Main');

        $data = [
            'name' => 'Test Name',
            'age' => '22',
            'news' => $main->getNews(),
        ];

        $this->view->render('Главня страница', $data);
    }

    public function test()
    {
        echo ('fd');
    }
}