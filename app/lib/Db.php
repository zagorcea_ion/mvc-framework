<?php
namespace app\lib;
use mysqli;

class Db
{
    private static $db = null;

    public static function getInstance(){
        if (self::$db === null){
            $config = require 'app/config/db.php';
            self::$db = new mysqli($config['host'], $config['user'], $config['password'], $config['dbname']);
//            self::$db =  new PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'] . '', $config['user'], $config['password']);
        }

        return self::$db;
    }

    public function __construct(){}
    public function __clone(){}
    public function __wakeup(){}
    
}