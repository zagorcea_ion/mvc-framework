<?php

return [
    ''  => [
        'controller' => 'main',
        'action'     => 'index'
    ],

    //route for admin product page
    'admin/product' => [
        'controller' => 'product',
        'action'     => 'index'
    ],

    'admin/product/form' => [
        'controller' => 'product',
        'action'     => 'form'
    ],

    //route for admin category page
    'admin/category' => [
        'controller' => 'category',
        'action'     => 'index'
    ],

    'admin/category/form' => [
        'controller' => 'category',
        'action'     => 'form'
    ],

    'account/login'  => [
        'controller' => 'account',
        'action'     => 'login'
    ],

    'news/show' => [
        'controller' => 'news',
        'action'     => 'show'
    ],
];