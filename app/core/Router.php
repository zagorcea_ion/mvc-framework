<?php
namespace app\core;

class Router
{
    protected $route = [];
    protected $params = [];

    public function __construct()
    {
        $arr = require 'app/config/routes.php';

        foreach ($arr as $key => $value){
            $this->add($key, $value);
        }
    }

    public function add($route, $params)
    {
        $route = '#^' . $route . '$#';
        $this->route[$route] = $params;     //сохраняем route
    }

    //метод проверяет если существукт введенный url в routes config
    public function match()
    {
        $url = trim($_SERVER['REQUEST_URI'], '/');      //получаем текущий url и подшотавлтваем его

        foreach ($this->route as $rout => $params){
            if (preg_match($rout, $url, $matches)){
                $this->params = $params;    //сохраняем params тоесть controller и action для найденного url
                return true;
            }
        }
        return false;
    }

    public function run()
    {
        //проверяем если маршрут существует
        if ($this->match()){

            //генирируем путь к контроллеру
            $path = 'app\controllers\\' . ucfirst($this->params['controller']) . 'Controller';
            //проверяем если контроллер существует

            if (class_exists($path)){

                $action = $this->params['action'];      //метод из класса
                //проверяем если метод существует
                if(method_exists($path, $action)){
                    $controller = new $path($this->params);    //создаем экземпляр класса
                    $controller->$action();     //вызываем метод
                }else{
                    View::errorCode(404);
                }

            }else{
                View::errorCode(404);
            }

        }else{
            View::errorCode(404);
        }
    }
}