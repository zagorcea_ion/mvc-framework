<?php

namespace app\core;
use app\core\View;

abstract class Controller
{

    public $route;
    public $view;

    public function __construct($route)
    {
        $this->route = $route;
        $this->view = new View($route);

        //задаем шаблон для контроллера
        if (method_exists($this,'setLayout')){
            $this->setLayout();
        }
    }

    public function loadModel($name)
    {
        $path = 'app\models\\' . ucfirst($name);

        if(class_exists($path)){
            return new $path;
        }
    }
}