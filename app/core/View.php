<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 19.02.2019
 * Time: 9:10
 */

namespace app\core;


class View
{
    public $patch;
    public $route;
    public $layout = 'default';

    public function __construct($route)
    {
        $this->route = $route;
        $this->patch = $route['controller'] . '/' . $route['action'];
    }

    public function render($title, $data = [])
    {
        extract($data);     //извлекаем наши данные
        $path = 'app/views/' . $this->patch . '.php';

        if(file_exists($path)){
            ob_start();
            require $path;               //загружаем вид в соответствии контроллкру и методу
            $content = ob_get_clean();
            require 'app/views/layouts/' . $this->layout . '.php';      //загружаем шаблон

        }
    }

    //redirect method
    public function redirect($url)
    {
        header('location' . $url);
    }

    //method for error page
    public static function errorCode($code)
    {
        http_response_code($code);
        $path = 'app/views/error/' . $code . '.php';

        if (file_exists($path)){
            require $path;
        }else{
            echo('Стрыница ' . $code);
        }
        exit;
    }

    public function message($status, $message)
    {
        exit(json_encode(['status' => $status, 'message' => $message]));
    }

    public function location($url)
    {
        exit(json_encode(['url' => $url]));
    }

}