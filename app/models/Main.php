<?php
namespace app\models;

use app\core\Model;
//use PDO;
use mysqli;
class Main extends Model
{
    public function query($sql, $params = [])
    {
        $stmt = $this->db->prepare($sql);

        if (!empty($params)){
            foreach ($params as $key => $val){
                $stmt->bindValue(':' . $key, $val);
            }
        }

        $stmt->execute();
        return  $stmt;
    }

    public function getNews($params = [])
    {
        $query = $this->db->query('SELECT title FROM news');
        $query->fetch_assoc();
        return $query;
//        $result = $this->query('SELECT title FROM news', $params);
//        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
}