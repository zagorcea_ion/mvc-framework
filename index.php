<?php

require ('app/lib/dev.php');

use app\core\Router;

//загружаем классы
spl_autoload_register(function ($class){
   $path = str_replace('\\', '/', $class . '.php');     //создаем правельный путь к классу

   if (file_exists($path)){     //провкпяем если такой фаил существует
       require $path;
   }
});

session_start();

$router = new Router();
$router->run();